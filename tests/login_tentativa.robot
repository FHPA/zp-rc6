***Settings***
Documentation   Login Tentativa

Resource    ../resources/base.robot
Resource    ../resources/kws.robot

#Executa uma ou mais Keywords somente uma vez antes da execução de todos os steps de cada caso de teste
Suite Setup     Start Session
#Executa uma ou mais Keywords somente uma vez após a execução de todos os steps de cada caso de teste
Suite Teardown  Finish Session

Test Template   Tentativa de login

***Test Cases***
Senha incorreta             admin@zepalheta.com.br      abc123          Ocorreu um erro ao fazer login, cheque as credenciais.
Senha em branco             joao@gmail.com              ${EMPTY}        O campo senha é obrigatório!    
Email em branco             ${EMPTY}                    123456          O campo email é obrigatório!
Email e senha em branco     ${EMPTY}                    ${EMPTY}        Os campos email e senha não foram preenchidos!
Login incorreto             admin&gmail.com             abc123          Ocorreu um erro ao fazer login, cheque as credenciais.
