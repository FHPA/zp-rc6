***Settings***
Documentation       Tentativa de cadastro de clientes - Campos Obrigatorios

Resource    ../resources/base.robot

Suite Setup     Login Session
Suite Teardown  Finish Session


Test Template   Cadastro de Clientes - Campos Obrigatórios


***Test Cases***
Nome é obrigatório          ${EMPTY}            00000014141     Rua dos bugs, 1000      62999999999     Nome é obrigatório
CPF é obrigatório           Fulano da Silva     ${EMPTY}        Rua dos bugs, 1000      62999999999     CPF é obrigatório
Endereço é obrigatório      Fulano da Silva     00000014141     ${EMPTY}                62999999999     Endereço é obrigatório
Telefone é obrigatório      Fulano da Silva     00000014141     Rua dos bugs, 1000      ${EMPTY}        Telefone é obrigatório

#Adicionalmente, testando cenários de cpf e telefone inválidos
CPF inválido                Fulano da Silva     0               Rua dos bugs, 1000      62999999999     CPF inválido
Telefone inválido           Fulano da Silva     00000014141     Rua dos bugs, 1000      6               Telefone inválido