***Settings***
Documentation       Representação da página login com suas ações e elementos


***Keywords***
Login With
    [Arguments]     ${email}     ${password}

   Input Text                          id:txtEmail                          ${email}
   Input Text                          css:input[placeholder=Senha]         ${password}
   Wait Until Element is Visible       xpath://button[text()='Entrar']  5
   Click Element                       xpath://button[text()='Entrar']