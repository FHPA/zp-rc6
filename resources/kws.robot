***Settings***

Resource    components/Toaster.robot
Resource    components/Sidebar.robot
Resource    pages/LoginPage.robot
Resource    pages/CustomersPage.robot

***Keywords***
#Login
Acesso a página Login
    Go To     ${base_url}
    
Submeto minhas credenciais
    [Arguments]     ${email}    ${password}

    Login With      ${email}    ${password}

Devo ver a área logada
    [Arguments]     ${output_mensagem}

    Wait Until Page Contains    ${output_mensagem}       5

Devo ver um toaster com a mensagem
    [Arguments]     ${expect_message}

    Wait Until Element Contains     ${TOASTER_ERROR}       ${expect_message}

Tentativa de login
    [Arguments]     ${input_email}      ${input_senha}      ${output_mensagem}

    Acesso a página Login
    Submeto minhas credenciais          ${input_email}      ${input_senha}
    Devo ver um toaster com a mensagem  ${output_mensagem}

Login com sucesso
    [Arguments]     ${input_email}      ${input_senha}      ${output_mensagem}
    
    Acesso a página Login
    Submeto minhas credenciais      ${input_email}      ${input_senha}
    Devo ver a área logada          ${output_mensagem}

#Customers
Dado que acesso o formulário de cadastro de clientes
    Wait Until Element Is Visible       ${NAV_CUSTOMERS}    5
    Click Element                       ${NAV_CUSTOMERS}
    Wait Until Element Is Visible       ${CUSTOMERS_FORM}   5
    Click Element                       ${CUSTOMERS_FORM}

Quando faço a inclusão desse cliente:
    [Arguments]                 ${name}     ${cpf}      ${address}      ${phone_number} 

    Remove Customer By cpf      ${cpf} 
    Register New Customer       ${name}     ${cpf}      ${address}      ${phone_number}

Então devo ver a notificação:
    [Arguments]     ${expect_notice}

    Wait Until Element Contains     ${TOASTER_SUCCESS}         ${expect_notice}   5

Cadastro de Clientes
    [Arguments]                 ${name}     ${cpf}      ${address}      ${phone_number}     ${expect_notice}
    Dado que acesso o formulário de cadastro de clientes
    Quando faço a inclusão desse cliente:
    ...         ${name}     ${cpf}      ${address}      ${phone_number}
    Então devo ver a notificação:       ${expect_notice}

Cadastro de Clientes - Campos Obrigatórios
    [Arguments]         ${name}     ${cpf}      ${address}      ${phone_number}     ${expect_text}

    Dado que acesso o formulário de cadastro de clientes
    Quando faço a inclusão desse cliente:       ${name}     ${cpf}      ${address}      ${phone_number}
    Devo ver o texto no corpo da página         ${expect_text}

Devo ver o texto no corpo da página
    [Arguments]                 ${expect_text}

    Wait Until Page Contains    ${expect_text}   5